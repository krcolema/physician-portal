export const users = [
    {
        "firstName": "Kavaruss",
        "lastName": "Coleman",
        "username": "test",
        "password": "test",
        "assignedPatients": [
            14785,
            95215,
            87521,
            63824,
            15934,
            65412,
            96385,
            12768,
            98465,
            21453,
            75324,
            88554
        ],
        "userId": 1
    },
    {
        "firstName": "Jesse",
        "lastName": "Wyatt",
        "username": "jwyatt",
        "password": "test",
        "assignedPatients": [
            21564
        ],
        "userId": 2
    }
]