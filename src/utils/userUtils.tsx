import { patientList as patients } from '../demoData/patients';
import { users } from '../demoData/users';
import { Patient } from '../models/patient';
import { User } from '../models/user';

const newPatientList: Patient[] = patients.map(patient => {
    return new Patient().deserialize(patient);
})


export const getPatients = (userId: number): Patient[] => {
    if (userId <= 0) {
        return [];
    }
    const foundUser = users.find(user => {
        return user.userId === userId;
    })

    let user: User = new User();

    if (foundUser) {
        user = new User().deserialize(foundUser);
    }

    const patients = newPatientList.filter((patient: Patient): Patient | undefined => {
        if (user.assignedPatients!.includes(patient.id!)) {
            return patient;
        }
    });

    if (patients) {
        return patients as Patient[];
    } else {
        return [];
    }

}

export const getUserById = (userId: number): User => {

    const foundUser = users.find(user => {
        return user.userId === userId;
    })

    let user: User = new User();

    if (foundUser) {
        user = new User().deserialize(foundUser);
    }

    return user;
}