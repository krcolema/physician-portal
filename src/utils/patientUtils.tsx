import { patientList } from '../demoData/patients';
import { Patient } from '../models/patient';

const newList: Patient[] = patientList.map(patient => {
    return new Patient().deserialize(patient);
})

export const getPatientInfo = (patientId: number): Patient => {
    if (patientId <= 0) {
        return new Patient();
    }

    const patient = newList.find((patient: Patient) => {
        return patient.id === patientId;
    });

    if (patient) {
        return patient;
    } else {
        return new Patient();
    }

}

