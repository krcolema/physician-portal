import { IonCard, IonCardHeader, IonCardTitle, IonContent, IonHeader, IonItem, IonList, IonListHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../State';
import { getUserById } from '../utils/userUtils';
import './SideMenu.scss';

export const SideMenu: React.FC = () => {
    const { firstName, lastName, logOut } = useAuth();


    const history = useHistory();

    const handleLogout = async () => {
        await logOut();
        history.replace('/login');
    }
    let userId = 1; //default user
    const loggedInUserId = localStorage.getItem('loggedInUserId');
    if (loggedInUserId !== null && loggedInUserId != '') {
        userId = parseInt(loggedInUserId!);
    }

    const user = getUserById(userId);
    if (!(userId === 1 || userId === 2)) {
        return <IonPage>
            <IonContent>
                Error getting user
            </IonContent>
        </IonPage>
    }
    return (
        <IonPage>

            <IonHeader>
                <IonToolbar>
                    <IonTitle>Netsmart Physician</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>

                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            {user.lastName}, {user.firstName}
                        </IonCardTitle>
                    </IonCardHeader>
                </IonCard>
                <IonList>
                    <IonItem >Profile</IonItem>
                    <IonItem >Settings</IonItem>
                </IonList>
                <IonList>
                    <IonListHeader>Account</IonListHeader>
                    <IonItem >Help</IonItem>
                    <IonItem >Logout</IonItem>
                </IonList>
            </IonContent>
        </IonPage>

    )
}
