import { IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs } from '@ionic/react';
import { calendar, personCircle } from 'ionicons/icons';
import { Redirect, Route, RouteComponentProps } from 'react-router';
import Chart from '../pages/Forms';
import Profile from '../pages/Profile';
import './PatientContainer.css';

interface PatientContainerProps extends RouteComponentProps<{
  patientId: string;
}> { }

interface PatientContainerParams {
  patientId: string;
}



const PatientContainer: React.FC<PatientContainerProps> = ({ match }) => {

  //const { patientId} = this!.props!;
  return (

    <IonTabs>
      <IonRouterOutlet id="patient">

        <Route path={`/patient/profile`} component={Profile} exact />
        <Route path={`/patient/forms`} component={Chart} exact />
        <Route exact path="/patient" render={() => <Redirect to={"/patient/profile"} />} />

      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton tab="chart" href={`/patient/profile`}>
          <IonIcon icon={calendar} />
          <IonLabel>Chart</IonLabel>
        </IonTabButton>

        <IonTabButton tab="forms" href={`/patient/forms`}>
          <IonIcon icon={personCircle} />
          <IonLabel>Forms</IonLabel>
        </IonTabButton>
      </IonTabBar>

    </IonTabs>

  );
};

export default PatientContainer;
