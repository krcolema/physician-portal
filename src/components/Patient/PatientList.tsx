import { IonList } from "@ionic/react";
import { IPatient, Patient } from "../../models/patient";
import PatientInfoCard from "./PatientInfoCard";
import './PatientList.scss'

interface PatientListProps {
    patients: IPatient[];
    onClickEvent: any;
}

const PatientList: React.FC<PatientListProps> = ({ patients, onClickEvent }) => {
    return (
        <IonList className="patientList">
            {
                patients.map((patient: IPatient) => {
                    return (
                        <span key={"pid" + patient.id!} onClick={(e) => {
                            e.preventDefault();
                            console.log(e);
                            onClickEvent(patient.id!);
                        }}>
                            <PatientInfoCard
                                firstName={patient.firstName!}
                                lastName={patient.lastName!}
                                admissionStatus={patient.admissionStatus!}
                                patientId={patient.id!.toString()}
                                patientCode={patient.patientCode!}
                                sex={patient.sex!}
                                age={patient.age!}
                                socDate={patient.socDate!}
                            />
                        </span>
                    )
                })
            }
        </IonList>
    );
};

export default PatientList;
