import { IonAccordionGroup, IonAccordion, IonItem, IonLabel, IonGrid, IonRow, IonCol, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import { getPatientInfo } from "../../utils/patientUtils";
import './PatientDemographics.scss';


const PatientDemographics: React.FC = () => {

    const patientStorageId = localStorage.getItem('patientId');
    if (patientStorageId == null || patientStorageId === '') {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Patient Card</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    No patient information available;
                </IonContent>
            </IonPage>
        )
    }
    const patientId = JSON.parse(patientStorageId) as number;
    const patientInfo = getPatientInfo(patientId) || null;

    const generateValue = (value: any) => {
        if (value == null || value === '') {
            return '(Not Available)';
        } else {
            return value;
        }
    }
    return (
        <IonAccordionGroup expand='inset' className="patientDemographics">
            <IonAccordion value="personalInfo">
                <IonItem slot="header" className="item-background-color">
                    <IonLabel>Personal Information</IonLabel>
                </IonItem>
                <IonGrid slot="content">
                    <IonRow>
                        <IonCol>
                            <div>
                                Date of Birth
                            </div>
                            <div>
                                {generateValue(patientInfo.dateOfBirth)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                Language Spoken
                            </div>
                            <div>
                                {generateValue(patientInfo.language)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                Email
                            </div>
                            <div>
                                {generateValue(patientInfo.email)}
                            </div>
                        </IonCol>
                        <IonCol>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <div>
                                Race/Ethnicity
                            </div>
                            <div>
                                {generateValue(patientInfo.race)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                SSN
                            </div>
                            <div>
                                {generateValue(patientInfo.ssn)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                Numbers
                            </div>
                            <div>
                                {generateValue(patientInfo.numbers)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                Patient Lives With
                            </div>
                            <div>
                                {generateValue(patientInfo.livesWith)}
                            </div>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <div>
                                Marital Status
                            </div>
                            <div>
                                {generateValue(patientInfo.maritalStatus)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                SOC Date
                            </div>
                            <div>
                                {generateValue(patientInfo.socDate)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                Address
                            </div>
                            <div>
                                {generateValue(patientInfo.address)}
                            </div>
                        </IonCol>
                        <IonCol>
                            <div>
                                Cross Street
                            </div>
                            <div>
                                {generateValue(patientInfo.crossStreet)}
                            </div>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <div>
                                Comments
                            </div>
                            <div>
                                {generateValue(patientInfo.comments)}
                            </div>
                        </IonCol>
                        <IonCol>
                        </IonCol>
                        <IonCol>
                        </IonCol>
                        <IonCol>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonAccordion>
        </IonAccordionGroup>
    );
};

export default PatientDemographics;
