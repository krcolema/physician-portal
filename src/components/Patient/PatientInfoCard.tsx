import { IonCard, IonCol, IonGrid, IonRow } from '@ionic/react';
import './PatientInfoCard.scss';

interface PatientInfoCardProps {
    firstName: string;
    lastName: string;
    admissionStatus: string;
    patientId: string;
    patientCode: string;
    sex: string;
    age: string;
    socDate: string;
}

const PatientInfoCard: React.FC<PatientInfoCardProps> = ({ firstName, lastName, admissionStatus, patientId, patientCode, sex, age, socDate }) => {
    return (
        <IonCard className="patientInfoCardContainer">
            <IonGrid>
                <IonRow>
                    <IonCol>
                        <span className='patientName'>{firstName} {lastName}</span> <span className='admissionStatus'>{admissionStatus}</span>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        {patientId !== '' ? patientId : patientCode} {sex} {age !== '' ? '| ' + age : ''}
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        {socDate !== '' ? 'Start Date ' + socDate : ''}
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonCard>
    );
};

export default PatientInfoCard;
