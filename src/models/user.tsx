export interface IUser {
    firstName?: string | undefined;
    lastName?: string | undefined;
    assignedPatient?: number[] | undefined;
    username?: string | undefined;
    userId?: number | undefined;
}

export class User implements IUser {
    firstName?: string | undefined;
    lastName?: string | undefined;
    assignedPatients?: number[] | undefined;
    username?: string | undefined;
    userId?: number | undefined;

    deserialize(input: any) {
        this.firstName = input.firstName;
        this.lastName = input.lastName;
        this.assignedPatients = input.assignedPatients;
        this.username = input.username;
        this.userId = input.userId;
        return this;
    }
}