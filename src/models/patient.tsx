export interface IPatient {
    firstName?: string | undefined;
    lastName?: string | undefined;
    dateOfBirth?: string | undefined;
    id?: number | undefined;
    language?: string | undefined;
    email?: string | undefined;
    race?: string | undefined;
    ssn?: string | undefined;
    numbers?: string | undefined;
    livesWith?: string | undefined;
    maritalStatus?: string | undefined;
    socDate?: string | undefined;
    address?: string | undefined;
    crossStreet?: string | undefined;
    comments?: string | undefined;
    age?: string | undefined;
    sex?: string | undefined;
    patientCode?: string | undefined;
    admissionStatus?: string | undefined;
}

export class Patient implements IPatient {
    firstName?: string | undefined = '';
    lastName?: string | undefined = '';
    dateOfBirth?: string | undefined = '';
    language?: string | undefined = '';
    id?: number | undefined = 0;
    email?: string | undefined = '';
    race?: string | undefined = '';
    ssn?: string | undefined = '';
    numbers?: string | undefined = '';
    livesWith?: string | undefined = '';
    maritalStatus?: string | undefined = '';
    socDate?: string | undefined = '';
    address?: string | undefined = '';
    crossStreet?: string | undefined = '';
    comments?: string | undefined = '';
    age?: string | undefined = '';
    sex?: string | undefined = '';
    patientCode?: string | undefined = '';
    admissionStatus?: string | undefined = '';

    deserialize(input: any) {
        this.firstName = input.firstName;
        this.lastName = input.lastName;
        this.dateOfBirth = input.dateOfBirth;
        this.language = input.language;
        this.id = input.id;
        this.email = input.email;
        this.race = input.race;
        this.ssn = input.ssn;
        this.numbers = input.numbers;
        this.maritalStatus = input.maritalStatus;
        this.socDate = input.socDate;
        this.address = input.address;
        this.crossStreet = input.crossStreet;
        this.comments = input.comments;
        this.age = input.age;
        this.sex = input.sex;
        this.patientCode = input.patientCode;
        this.admissionStatus = input.admissionStatus;
        return this;
    }
}