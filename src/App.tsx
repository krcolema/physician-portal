import {
  IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
import '@ionic/react/css/display.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/float-elements.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/typography.css';
// Netsmart UI
import { allComponents, provideNtstDesignSystem } from '@ntstforms/netsmart-ui';
import { homeSharp, menuSharp } from 'ionicons/icons';
import { Redirect, Route } from 'react-router-dom';
import './App.scss';
import { SideMenu } from './components/SideMenu';
import PatientDashboard from './pages/PatientsDashboard';
import Profile from './pages/Profile';
import { AuthContextProvider, useAuth } from './State';
/* Theme variables */
import './theme/variables.css';

setupIonicReact();

provideNtstDesignSystem().register(Object.values(allComponents).map((comp) => comp()))

const App: React.FC = () => {
  const { loggedIn } = useAuth();
  return (
    <IonApp className='wrapper'>
      <AuthContextProvider>
        <>
          <IonReactRouter>
            <IonTabs>
              <IonRouterOutlet id="main">

                <Route path={`/dashboard`} component={PatientDashboard} exact />
                <Route path={`/menu`} component={SideMenu} exact />
                {/* <Route exact component={Login} path="/login" /> */}
                <Route path={`/patient/profile`} component={Profile} exact />
                <Route exact path="/" render={() => <Redirect to={"/dashboard"} />} />

              </IonRouterOutlet>
              <IonTabBar slot="bottom">
                <IonTabButton tab="dashboard" href={`/dashboard`}>
                  <IonIcon icon={homeSharp} />
                  <IonLabel>Dashboard</IonLabel>
                </IonTabButton>

                <IonTabButton tab="menu" href={`/menu`}>
                  <IonIcon icon={menuSharp} />
                  <IonLabel>Menu</IonLabel>
                </IonTabButton>
              </IonTabBar>

            </IonTabs>
          </IonReactRouter>
        </>
      </AuthContextProvider>

    </IonApp>
  )
};

export default App;
