import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import PatientDemographics from '../components/Patient/PatientDemographics';
import { getPatientInfo } from '../utils/patientUtils';
import './Profile.css';

const Profile: React.FC = () => {
  const patientStorageId = localStorage.getItem('patientId');
  console.log(patientStorageId);
  if (patientStorageId == null || patientStorageId === '') {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Patient Card</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          No patient information available;
        </IonContent>
      </IonPage>
    )
  }
  const patientId = JSON.parse(patientStorageId) as number;
  const patientInfo = getPatientInfo(patientId) || null;

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Patient Card</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard>
          <IonCardHeader>
            <IonCardTitle>
              {patientInfo.lastName}, {patientInfo.firstName}
            </IonCardTitle>
          </IonCardHeader>
        </IonCard>
        <IonCard>
          <IonCardContent>
            <PatientDemographics />
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Profile;
