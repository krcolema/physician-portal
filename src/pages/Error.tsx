import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Error.css';

const ErrorPage: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>404 Not Found</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <div>
                    404 Not Found
                </div>
            </IonContent>
        </IonPage>
    );
};

export default ErrorPage;
