import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, useIonToast } from '@ionic/react';
import { useHistory } from 'react-router-dom';
import { users } from '../demoData/users';
import { IUser } from '../models/user';
import { useAuth } from '../State';
import './Login.scss';

const validateUser = (username: string, password: string) => {
  const validUser = users.find((value: { username: string; password: string; }) => {
    return value.username === username && value.password === password;
  })
  return validUser;
}

const Login: React.FC = () => {
  const { logIn } = useAuth();
  const { loggedIn } = useAuth();
  console.log('Login page:' + loggedIn);
  const history = useHistory();
  const [present, dismiss] = useIonToast();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className='wrapper'>
          <span>
            <ntst-text-input label={"Username"} id="username"
            ></ntst-text-input>
            <ntst-text-input label={"Password"} id="password"
            ></ntst-text-input>
          </span>
          <br />
          <br />
          <div className='loginWrapper'>
            <span onClick={async (e) => {
              e.preventDefault();
              const username = (document.getElementById('username') as any).value;
              const password = (document.getElementById('password') as any).value;
              const validatedUser = validateUser(username, password);
              if (validatedUser != null) {
                const user: IUser = validatedUser;
                await logIn(user);
                history.replace('/dashboard');
              } else {
                console.log('Not a valid user. ry using username/password: test/test')
              }
            }}>
              <ntst-button >
                Login

              </ntst-button>
            </span>
            <div className="pointer" onClick={() => {
              present({
                buttons: [{ text: 'Dismiss', handler: () => dismiss() }],
                message: 'Try using username/password: test/test'
              })

            }}>
              <ntst-icon library="fas" icon="info" iconColor="#003b5c">
              </ntst-icon>
            </div>
          </div>
        </div>
      </IonContent>



    </IonPage >
  );
};

export default Login;
