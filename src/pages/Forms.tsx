import { IonButtons, IonCard, IonCardContent, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import {
  provideFASTDesignSystem
} from '@microsoft/fast-components';
import { provideReactWrapper } from '@microsoft/fast-react-wrapper';
import { ntstDataGrid } from '@ntstforms/netsmart-ui';
import React from 'react';
import './Forms.css';

const { wrap } = provideReactWrapper(
  React,
  provideFASTDesignSystem()
);

export const DataGrid = wrap(ntstDataGrid());

const Chart: React.FC = () => {

  const patientStorageId = localStorage.getItem('patientId');
  console.log(patientStorageId);
  if (patientStorageId == null || patientStorageId === '') {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Patient Card</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          No patient information available;
        </IonContent>
      </IonPage>
    )
  }
  const patientId = JSON.parse(patientStorageId) as number;

  const dataSource = (dataInfo: any) => {
    console.log(dataInfo);
    const source = [{
      form: '1',
      date: '1',
      user: '1',
      status: '1',
      episode: '1',
      edit: '1',
      delete: '1'
    }, {
      form: '2',
      date: '2',
      user: '2',
      status: '2',
      episode: '2',
      edit: '2',
      delete: '2'
    }]
    if (dataInfo.sortDirection === "DESC") {
      source.sort((a: any, b: any) => {
        return b[dataInfo.sortField] - a[dataInfo.sortField]
      });
    } else {
      source.sort((a: any, b: any) => {
        return a[dataInfo.sortField] - b[dataInfo.sortField]
      });
    }
    return {
      data: source,
      totalCount: source.length
    }
  }

  // const ace: DataSourceInfo
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Patient Card</IonTitle>
          <IonButtons slot="end">
            <IonMenuButton autoHide={true}></IonMenuButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard>
          <IonCardContent>
            <DataGrid datasource={dataSource}>
              <ntst-data-column
                headerText="Form"
                dataValue="form"
                sortable
              ></ntst-data-column>
              <ntst-data-column
                headerText="Date"
                dataValue="date"
                sortable
              ></ntst-data-column>
              <ntst-data-column
                headerText="User"
                dataValue="user"
                sortable
              ></ntst-data-column>
              <ntst-data-column
                headerText="Status"
                dataValue="status"
                sortable
              ></ntst-data-column>
              <ntst-data-column
                headerText="Episode"
                dataValue="episode"
                sortable
              ></ntst-data-column>
              <ntst-data-column
                headerText="Edit"
                dataValue="edit"
                sortable
              ></ntst-data-column>
              <ntst-data-column
                headerText="Delete"
                dataValue="delete"
                sortable
              ></ntst-data-column>
            </DataGrid>
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Chart;

