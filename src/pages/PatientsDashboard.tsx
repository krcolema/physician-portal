import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonPage, IonRow, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import { FormEvent, useState } from 'react';
import { useHistory } from 'react-router-dom';
import PatientList from '../components/Patient/PatientList';
import { getPatients } from '../utils/userUtils';
import './PatientsDashboard.scss';


const PatientsDashboard: React.FC = () => {
  const history = useHistory();
  const handlePatientClick = (patientId: number) => {
    localStorage.setItem('patientId', patientId.toString());
    history.push({
      pathname: `/patient/profile`,
      state: { patientId: patientId }
    })
  }
  //const { userId } = useAuth();
  const userId = 1;
  const allPatients = getPatients(userId);
  const [filteredPatientList, setFilteredPatientList] = useState(allPatients);
  const [searchText, setSearchText] = useState('');
  return (
    <IonPage>

      <IonHeader>
        <IonToolbar>
          <IonTitle>Netsmart Physician</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonGrid>
          <IonCard>
            <IonHeader>
              <IonRow>
                <IonCol size="6">
                  <IonCardHeader>
                    <IonCardTitle>
                      Assigned Patients
                    </IonCardTitle>
                  </IonCardHeader>
                </IonCol>
                <IonCol size="6">
                  <IonSearchbar placeholder='Search Patients' value={searchText} onIonChange={e => {
                    const query = e.detail.value!.toLowerCase();
                    setSearchText(e.detail.value!);
                    const filteredList = allPatients.filter((patient) => {
                      return (patient.firstName!.toLowerCase().indexOf(query) > -1) || (patient.lastName!.toLowerCase().indexOf(query) > -1);
                    })
                    setFilteredPatientList(filteredList);
                  }
                  }></IonSearchbar>
                </IonCol>
              </IonRow>

            </IonHeader>
          </IonCard>
          <IonCardContent>
            <PatientList patients={filteredPatientList} onClickEvent={handlePatientClick} />
          </IonCardContent>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default PatientsDashboard;
