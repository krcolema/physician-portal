import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import auth from './Auth';

const PrivateRoute = ({ component: Component, ...rest }: any) => {
    return (

        // Show the component only when the user is logged in
        // Otherwise, redirect the user to /login page
        <Route {...rest} render={props => (
            auth.isAuthenticated() ?
                <Component {...props} />
                : <Redirect to="/" />
        )} />
    );
};

export default PrivateRoute;