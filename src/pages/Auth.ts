class Auth {

    private authenticated: boolean;

    constructor() {
        this.authenticated = false;
    }

    login(callback: Function) {
        this.authenticated = true;
        callback();
    }

    logout(callback: Function) {
        this.authenticated = false;
        callback();
    }

    isAuthenticated() {
        return this.authenticated;
    }
}

export default new Auth();