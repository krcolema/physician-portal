import React, { Component, Props } from 'react';
import { Route, Redirect } from 'react-router-dom';
import auth from './Auth';

const ProtectedRoute = ({ component: Component, ...rest }: any) => {
    return (
        // restricted = false meaning public route
        // restricted = true meaning restricted route
        <Route {...rest} render={props => (
            !auth.isAuthenticated() ?
                <Redirect to="/" />
                : <Component {...props} />
        )} />
    );
};

export default ProtectedRoute;