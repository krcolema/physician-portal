declare namespace JSX {
    interface IntrinsicElements {
        "ntst-button": any;
        "ntst-text-input": any;
        "ntst-icon": any;
        "ntst-datagrid": any;
        "ntst-data-column": any;
    }
}