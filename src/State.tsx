import React, { createContext } from "react";
import { IUser } from "./models/user";
export interface IAuthContext {
    loggedIn: boolean,
    username: string,
    firstName: string,
    lastName: string,
    userId: number;
    logIn: any;
    logOut: any;
}

const defaultAuthInfo: IAuthContext = {
    loggedIn: true,
    username: 'test',
    firstName: 'Kavaruss',
    lastName: 'Coleman',
    userId: 1,
    logIn: () => { },
    logOut: () => { }
}


const AuthContext = createContext<IAuthContext>(defaultAuthInfo);


export const AuthContextProvider: React.FC = ({ children }) => {
    const [loggedIn, setLoggedIn] = React.useState<any>();
    const [username, setUsername] = React.useState<any>();
    const [firstName, setFirstName] = React.useState<any>();
    const [lastName, setLastName] = React.useState<any>();
    const [userId, setUserId] = React.useState<any>();

    const logIn = (user: IUser) => {
        return new Promise((resolve) => {
            setLoggedIn(true);
            setUsername(user.username);
            setFirstName(user.firstName);
            setLastName(user.lastName);
            setUserId(user.userId);
            localStorage.setItem('patientId', '');
            setTimeout(() => {
                return resolve(true);
            }, 1000);
        });
    }

    const logOut = () => {
        return new Promise((resolve) => {
            setLoggedIn(false);
            setUsername('');
            setFirstName('');
            setLastName('');
            setUserId('');
            localStorage.setItem('patientId', '');
            return resolve(true);
        });
    }

    let v = {
        username,
        firstName,
        lastName,
        loggedIn,
        userId,
        logIn: logIn,
        logOut: logOut
    }
    return <AuthContext.Provider value={v}>{children}</AuthContext.Provider>
}

export const useAuth = () => React.useContext(AuthContext) as IAuthContext;